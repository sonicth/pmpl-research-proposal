\documentclass[a4paper]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage
	% [draft]
	{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\title{Research Proposal\\
	Based on Master Thesis:\\
	Procedural Modelling of Park Layouts}
\author{Mike Vasiljevs}

\begin{document}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
A system for park synthesis using rule-based layout generation was described in Master's thesis \cite{VasiljevsMasters2018}. 
Research goals are, besides describing some of the thesis methods and showing more examples, is to improve these methods and, possibly, to generalise the differences between them.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

The thesis \cite{VasiljevsMasters2018} describes a procedural modelling system for parks, which works by first recursively partitioning the input region, such as a block or a quarter produced in CityEngine, and and then distributing vegetation within the resulted subregions.
Novelty in this thesis work is first, the observation of layouts patterns in the real-life parks, and then implementing these patterns into the CGA-inspired shape grammar \cite{Cga2006}, and second, the indexing system that assists the partitioning (see Figure~\ref{fig:patterns_n_indexing}).

Possible research aims are to provide a better formalisation as well as to attempt to make generalisations between the methods. 
In other words, could the developed technology have a more rigorous theoretical foundation in order to be simpler, be applicable to a larger pool of park designs, and also, if more commonality could be found between the implemented patterns, simplifying the implementation further. 
This research should also include a second iteration of analysis of park patterns since now we can use the evaluated results to make improvements. We also would like to see if the existing system can create a larger pool of models that resemble real-life parks.

The next following Sections \ref{sec:improvement_voronoi} and \ref{sec:improvement_rays}, we briefly describe the possible research goals.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Curved Boundary Region Partitioning}
\label{sec:improvement_voronoi}

Regions with curved boundaries (Cells rule) are created by smoothing the Voronoi Diagram cells.
Moreover, regions are observed to be limited to a `blob’-like appearance, since they are rounded convex shapes, whereas most parks, including the CityEngine garden scene example (see Figure~\ref{fig:ce-garden}) feature concave boundary sections, as well as straight paths segments.
Changes to the Voronoi method, for instance changing the distance metric, could be used to deform the cells in order to produce a variety of shapes found in real life layouts.
%also weights, power diagram (which is changing what...?)
 

 \subsection{Improvements}

 A problem with paths becoming too small is visible in the results (see Figure~\ref{fig:cells-parks}). 
 A solution for this could be to merge a pair of regions, if after smoothing their common path becomes thinner than a given threshold value, say that of a minimum path width attribute.

 
 \subsection{Post-processing of Voronoi Cells}

 Currently, the only way a cell  shape can be modified is by smoothing.
 A smoothing domain could also be controlled, 
	%This is again where Houdini implementation becomes handy
 	for instance, limiting it to a given distance from a cell corner.
Besides smoothing, more controlled modification of the boundary can be attempted.


 \subsection{Generalisation}

 Voronoi Diagrams could also be used to create Grid layouts, by pacing the input samples at regular intevals. %also houdini job
 Since the Voronoi method is already used for Rays construction, doing this should achieve generalisation of the algorithms for all of the three patterns.


\begin{figure}
	\centering
	\includegraphics[height=0.3\textwidth]{graphics/01indexing-grid.pdf}
	\includegraphics[height=0.3\textwidth]{graphics/01indexing-cells.pdf}
	\includegraphics[height=0.3\textwidth]{graphics/indexing-rays.pdf}
	\caption{
		The layout generator implements three patterns observed in real-life and modelled parks: 
		Left: Grid -- based on grid-like intersection of roughly orthogonal park paths, set at regular intervals,
		Middle: Cells -- represents curved boundary region partitioning in a similar way to the arrangement of the biological cells and
		Right: Rays -- park paths can be though of as `casted' from a path junction towards the input region boundary; we considered allowing multiple of junctions in the single layout, which are joined between each other with path sections.
		Edge ranges are also showned to be labelled using `01'-indexing.
	}
	\label{fig:patterns_n_indexing}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.46\textwidth]{graphics/results_cells1.jpg}
\caption{below middle of park paths come too close together. The adjacent regions could be merged.}
\label{fig:cells-parks} 
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.46\textwidth]{graphics/cityengine_garden.jpg}
	\caption{CityEngine Garden example scene, courtesy of Esri.}
	\label{fig:ce-garden} 
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Path connections in Rays rule}
\label{sec:improvement_rays}

While the user specifies path sections that reach the entrances, the internal path sections between the junctions are created by the Delaunay Triangulation method. Such a layout looks mesh-like, and we would like to create more varied layouts.
This problem could be considered as a more specialised case of the street network generation, so, for example, trying different patterns in the modified L-Systems of Parish and Mueller ~\cite{parishmueller2001} could achieve more variation of layout designs. 
Other related methods, like fitting prefabricated patterns of paths~\cite{UrbanPattern2013}, could be considered.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Change of Implementation}

The implementation consists of plugins for Trimble SketchUp, chosen because of simplicity and presence of the components library. 
The generator library in the plugin is developed to be monolithic. This raises a problem in further development for both the research and the commercial use since it is not possible to replace a component without rebuilding the whole library.

Now that the pipeline is defined, the benefits of porting it over to Side Effects Houdini are more clearly visible.
The node-based network allows splitting the pipeline into operation nodes, which has substantial benefits since we can easily modify and replace individual operations.
This way the individual components are also automatically decoupled.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Next Steps}


\begin{description}
	
	\item [Stage1: Refinement of the current implementation; \emph{required}.]
	The immediate goal is to finish `Incomplete Features' mentioned in the subsection of the Conclusion chapter and then to create a more extensive example collection of more complex real-life parks.
	Add conditional operators.

	\item [Stage2: More advanced methods; \emph{preferable, assuming time}.]
	Try incorporating some of the above-suggested methods/algorithms.

	\item [Stage3: Application in the field; \emph{optional}.]
	Get in touch with landscape architecture professionals and ask them to evaluate the prototype.
	Research the landscape architecture principles, comparing our methods to the ones commonly used in the field.
\end{description}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \todo[inline, color=green!40]{Describe other ideas and tidy up.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{alpha}
\bibliography{layout-generation}

\end{document}